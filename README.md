# Trello Exporter

Instructions:

1. Add a bookmark to your browser bar. Change the title to "Export Trello" and change the address to this:
	
	javascript:var%20s=document.createElement('script');s.setAttribute('src',%20'http://localhost/~gkoehler/Export-Trello/export-trello.js');document.getElementsByTagName('body')%5B0%5D.appendChild(s);void(s);

2. You will want to substitute the URL for the path to this file on your local machine.

3. Open the trello board you want to export. Hit "Options", "Share, Print and Export." Press the bookmarklet and a new button will appear under "Export Data." Hit that button and you've got your export!

