// ask for the json url

//var jsonExportURL = prompt('JSON Export URL', '');
// test: https://trello.com/board/4faeac9f3e12096f5b118b06/camprestore.json

/* tried to grab it myself, but won't work for some reason.
var arExportLink = $('.button.js-export-json.js-real-link');
if(arExportLink.length == 0) {
	alert("Please open the 'Share, Print and Export' panel so this script can grab the URL for the JSON.");
} else {
	var url = arExportLink[0].src;
	console.log('loading ' + url);
	$.ajax({
		url:$(arExportLink[0]).attr("src")
	}).done(function(data) {
		console.log('done');
		console.log(data);
	});
}
*/

// add a button to the "export" panel
var newButton = $('<a style="margin: 0 4px 4px 0;" class="button" href="#" target="_blank">Rich Text</a>');

$(newButton).click(function(){
	fcnRunExport($('.js-export-json').attr('href'));
	return false;
});

// appending next to: <a style="margin: 0 4px 4px 0;" class="button js-export-json js-real-link" href="/board/4faeac9f3e12096f5b118b06/camprestore.json" target="_blank">JSON</a>
$('.js-export-json').parent().append(newButton);

var oExportData = null;

var oExportDoc = null;
var windowRef = null;

var fcnRunExport = function(jsonExportURL) {
	if(jsonExportURL) {
		windowRef = window.open(document.URL + '','mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
		
		$.ajax({
			url:jsonExportURL,
			dataType:'json'
		}).done(function(data) {
			oExportData = data;
			
			// ok, we've got a bunch of properties here.
			// actions, cards, checklists, invitations, lists, members are all arrays
			$(oExportData.lists).each(function(){
				listId = this.id;
				windowRef.document.write('<h3>'+this.name+'</h3>');
			
				// find the cards
				windowRef.document.write('<ol>');
				$(oExportData.cards).each(function(){
					if(this.idList == listId) {
						cardId = this.id;
						windowRef.document.write('<li><b>'+this.name+'</b><br />');
						windowRef.document.write('<p>'+this.desc+'</p>');
					
						// get the activity for the card. Difficult.
						$(oExportData.actions).each(function(){
							if(this.type=="commentCard" && this.data.card.id==cardId) {
								action = this;
								var d= new Date(this.date);
								windowRef.document.write('<p><b>Update ' + d.getMonth() + '-' + d.getDate() + ':</b> '+this.data.text+'</p>');
							}
						});
					
						windowRef.document.write('</li>');
					}
				});
				windowRef.document.write('</ol>');
			});	
		});
	}	
}
